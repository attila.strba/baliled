#ifndef OTA_H
#define OTA_H

#include "settings.h"
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

void init_ota();
void ota_handle();

#endif /* OTA_H */
