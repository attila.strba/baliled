#ifndef SETTINGS_H
#define SETTINGS_H

/* Parameters setup */
extern const int DARKNESS;
extern const unsigned long led_on_seconds;

/* Pin Setttings */
extern const int esp_builtin_led;
extern const int relay_led;
extern const int relay_reserve;
extern const int sensor_big;
extern const int sensor_small;

/* Constants setup */
#define LED_OFF HIGH
#define LED_ON LOW

/* Wifi Settings */
extern const char *ssid;
extern const char *password;

#endif /* SETTINGS_H */
