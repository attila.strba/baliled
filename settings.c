#include "settings.h"

/* Parameters setup */
const int DARKNESS = 1023;
const unsigned long led_on_seconds = 5;

/* Pin Setttings */
const int esp_builtin_led = 2; // Digital pin D0
const int relay_led = 14;      // Digital pin D5
const int relay_reserve = 12;  // Digital pin D6
const int sensor_big = 4;      // Digital pin D2
const int sensor_small = 5;    // Digital pin D1

/* Wifi Settings */
const char *ssid = "setmyssid";
const char *password = "";
