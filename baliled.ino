
#include "ota.h"
#include "settings.h"
// Whole Home assistant example:
// https://github.com/bruhautomation/ESP-MQTT-JSON-Digital-LEDs ESP8266
// https://arduino-esp8266.readthedocs.io/en/latest/faq/a01-espcomm_sync-failed.html
// PIR: https://github.com/MKme/Wemos-D1-ESP8266-PIR-Alarm
// test OTA
// https://randomnerdtutorials.com/esp8266-ota-updates-with-arduino-ide-over-the-air/
// https://www.instructables.com/id/Quick-Start-to-Nodemcu-ESP8266-on-Arduino-IDE/
// To upload over OTA:https://steve.fi/Hardware/ota-upload/
// python2 /home/$USER/Downloads/espota.py -d -i 192.168.1.239  -f
// ~/projects/baliled/build/baliled.ino.bin

#define CHIPSET WS2811

// Timer: Auxiliary variables
unsigned long now = millis();
unsigned long lastTrigger = 0;
typedef enum { ST_NO_MOVEMENT, ST_DETECT_MOVEMENT, ST_LIGHT_ON } state_codes_t;
state_codes_t state;

int read_light_intensity() {
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V)
  /* float voltage = sensorValue * (5.0 / 1023.0); */
  int sensor_value = analogRead(A0);
  /* Serial.println("Light sensor value"); */
  /* Serial.println(sensor_value); */
  return sensor_value;
}

// Checks if motion was detected
ICACHE_RAM_ATTR void detectsMovementBigSensor() {
  Serial.println("BIG SENSOR MOTION DETECTED!!!");
  lastTrigger = millis();
  if (state == ST_NO_MOVEMENT)
    state = ST_DETECT_MOVEMENT;
}

// Checks if motion was detected
ICACHE_RAM_ATTR void detectsMovementSmallSensor() {
  Serial.println("SMALL SENSOR MOTION DETECTED!!!");
  lastTrigger = millis();
  if (state == ST_NO_MOVEMENT)
    state = ST_DETECT_MOVEMENT;
}

void setup() {
  Serial.begin(115200);
  pinMode(esp_builtin_led, OUTPUT);
  Serial.println("Booting");
  digitalWrite(esp_builtin_led, LED_ON);
  init_ota();

  Serial.println("Setup Ready");
  pinMode(relay_led, OUTPUT);
  pinMode(relay_reserve, OUTPUT);
  pinMode(sensor_small, INPUT); // declare sensor as input
  pinMode(sensor_big, INPUT);   // declare sensor as input
  // Set motionSensor pin as interrupt, assign interrupt function and set RISING
  // mode
  attachInterrupt(digitalPinToInterrupt(sensor_big), detectsMovementBigSensor,
                  RISING);

  attachInterrupt(digitalPinToInterrupt(sensor_small),
                  detectsMovementSmallSensor, RISING);

  digitalWrite(relay_led, LED_OFF);
  digitalWrite(relay_reserve, LED_OFF);
  state = ST_NO_MOVEMENT;
}

void loop() {
  // Current time
  now = millis();
  switch (state) {
  case ST_NO_MOVEMENT:
    break;

  case ST_DETECT_MOVEMENT: {
    Serial.println(state);
    if (read_light_intensity() >= DARKNESS) {
      digitalWrite(relay_led, LED_ON);
      state = ST_LIGHT_ON;
      Serial.println("turn on ligh");
    } else
      state = ST_NO_MOVEMENT;
    break;
  };

  case ST_LIGHT_ON: {
    Serial.println(state);
    Serial.println(now);
    Serial.println(lastTrigger);
    if (digitalRead(sensor_small) == HIGH || digitalRead(sensor_big) == HIGH) {
      lastTrigger = millis();
      now = millis();
    }
    if ((unsigned long)(now - lastTrigger) >
        (unsigned long)(led_on_seconds * 1000)) {
      digitalWrite(relay_led, LED_OFF);
      Serial.println("turn off light");
      Serial.println(now);
      Serial.println(lastTrigger);
      state = ST_NO_MOVEMENT;
    }
    break;
  };
  }
  ota_handle();
}
